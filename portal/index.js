const express = require('express');
const app = express();
const dotenv = require('dotenv');
const authRoute = require('./routes/auth');
const openingRoute= require('./routes/opening');
const employeeRoute= require('./routes/employee');
const fileUpload = require('express-fileupload');

dotenv.config();

const mongoose = require('mongoose');

mongoose.connect(process.env.CONNECTION_STRING,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => {
        console.log('connected to db');
    });

//MiddleWare
app.use(express.json());
app.use(fileUpload());
//Handle route middleweres
app.use('/api/user', authRoute);
app.use('/api/opening', openingRoute);
app.use('/api/employee', employeeRoute);


app.listen(3000, () => console.log('Server up and running'));