const router = require('express').Router();
const userVerificationMiddleeware = require('../middleware/verifyUser');

router.post('/resume', userVerificationMiddleeware, async (req, res) => {

    const user = req.user;
    if (user.role !== 1) res.status(401).send('You are not authorised to perform this action');


    const file = req.files.doc;
    if (file.name.indexOf('.docx') === -1) res.status(401).send('Only Docx files are supported');

    file.mv('./resumes/' + user._id + '.docx', function (err, result) {
        if (err) {
            throw err;
        }
        res.send({ "success": true, "message": "File uploaded successfully" });
    })
});

router.get('/resume', userVerificationMiddleeware, async (req, res) => {

    const user = req.user;
    if (user.role !== 1) res.status(401).send('You are not authorised to perform this action');

    const file = `./resumes/${user._id}.docx`;
    res.download(file); 
});
module.exports = router;