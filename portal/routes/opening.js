const router = require('express').Router();
const User = require('../model/User');
const Opening = require('../model/Opening');
const userVerificationMiddleeware = require('../middleware/verifyUser');

router.post('/add', userVerificationMiddleeware, async (req, res) => {

    const user = req.user;
    if (user.role !== 0) res.status(401).send('You are not authorised to perform this action');
    const opening = new Opening({
        projectName: req.body.projectName,
        client: req.body.client,
        technologies: req.body.technologies,
        role: req.body.role,
        description: req.body.description,
        status: req.body.status
    })
    try {
        const savedOpening = await opening.save();
        res.status(200).send(savedOpening);
    } catch (err) {
        res.send(400).send(err);
    }
});

router.get('/all', userVerificationMiddleeware, async (req, res) => {
    try {
        const allOpenings = await Opening.find({ status: 1 });
        res.status(200).send(allOpenings);
    } catch (err) {
        res.send(400).send(err);
    }
});
router.get('/:id', userVerificationMiddleeware, async (req, res) => {
    const openingId = req.params.id;
    try {
        const opening = await Opening.findOne({ _id: openingId });
        res.status(200).send(opening);
    } catch (err) {
        res.send(400).send(err);
    }
});

router.delete('/:id', userVerificationMiddleeware, async (req, res) => {
    const user = req.user;
    if (user.role !== 0) res.status(401).send('You are not authorised to perform this action');

    const openingId = req.params.id;
    try {
        const opening = await Opening.deleteOne({ _id: openingId });
        res.status(200).send(opening);
    } catch (err) {
        res.send(400).send(err);
    }
});

router.patch('/:id', userVerificationMiddleeware, async (req, res) => {
    const user = req.user;
    if (user.role !== 0) res.status(401).send('You are not authorised to perform this action');

    const openingId = req.params.id;
    try {
        const opening = await Opening.update({ _id: openingId }, { status: 0 }); // no toggle reuired here
        res.status(200).send(opening);
    } catch (err) {
        res.send(400).send(err);
    }
});

router.patch('/:id/apply', userVerificationMiddleeware, async (req, res) => {

    const user = req.user;
    if (user.role !== 1) res.status(401).send('You are not authorised to perform this action');

    const openingId = req.params.id;
    const opening = await Opening.findOne({ _id: openingId });
    if(!opening) res.status(400).send("This is not a valid opening or is not accepting applications right now");
    let aplicants = opening.applicants;
    if(aplicants.indexOf(user._id)!=-1) {res.status(400).send('You have already applied for this opening');return;}
    aplicants.push(user._id);

    try {
        const openingUpdated = await Opening.updateOne({ _id: openingId }, { applicants: aplicants });
        res.status(200).send(openingUpdated);
    } catch (err) {
        res.status(400).send(err);
    }
});
module.exports = router;