const router = require('express').Router();
const User = require('../model/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/register', async (req, res) => {

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);
    const user = new User({
        id: req.body.id,
        name: req.body.name,
        email: req.body.email,
        password: hashPassword,
        role: req.body.role
    })
    try {
        const savedUser = await user.save();
        res.status(200).send(savedUser);
    } catch (err) {
        res.send(400).send(err);
    }
});


//log in
router.post('/login',async (req,res)=>{
    //check email 
    const user = await User.findOne({email : req.body.email});
    if(!user) return res.status(400).send('Email or password is incorrect');
    //check password
    const isPasswordValid = await bcrypt.compare(req.body.password,user.password);
    if(!isPasswordValid) return res.status(400).send('Email or password is incorrect');

    const token = jwt.sign({_id: user._id, role: user.role},process.env.TOKEN_SECRET);

    res.header('auth-token',token).send(token);
});

module.exports = router;