const mongoose = require('mongoose');

const openingSchema = new mongoose.Schema({
    projectName: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    client: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    technologies: {
        type: [String],
        required: true,
        min: 6,
        max: 1024
    },
    role: {
        type: String,
        enum: ['developer', 'tester', 'pceo'],
        default: 'developer'
    },
    description: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    status: {
        type: Number, //0 for closed and 1 for open
        default: 1
    },
    applicants : {
        type: [String],
        required: true,
        default: []
    }
});

module.exports = mongoose.model('Opening', openingSchema);