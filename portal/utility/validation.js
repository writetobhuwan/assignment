const Joi = require('@hapi/joi');

const registerValidation = input=>{
    const schema= {
        name: Joi.string().min(6).max(255).required(),
        password : Joi.string.min(6).max(255).required(),
        email :Joi.string.min(6).required().email()
    };
    return Joi.validate(data,schema);
}