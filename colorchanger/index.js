let app = require('express')();
let http = require('http').createServer(app);
let io = require('socket.io')(http);
let counter = require('./util/countdown');
let color = require('./util/colorgenerator');
let interval = 1000;


setInterval(() => {
    let counterValue = counter.countdown();
    io.to('room').emit('timerFired', counterValue, color());
}, interval)
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});
io.on('connection', (socket) => {
    socket.join('room');
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});
