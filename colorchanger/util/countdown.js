let currentCounter =6;
const countdown = () => {
    if (currentCounter === 0) {
        currentCounter = 6;
    }
    currentCounter -= 1;
    return currentCounter;
}

module.exports = {countdown};